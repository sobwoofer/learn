<?php

namespace app\models;
use Yii;
use yii\db\Query;

class Mylist extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		$sql = 'oc_product_description';
		

		return $sql;
	}

	public static function getAll($filter_data)
	{
		// $results = self::find()
		// 	->orderBy('name')
		// 	->offset($filter_data['pagination']->offset)
		// 	->limit($filter_data['pagination']->limit)
		// 	->all();

		$query = new Query();
		$results = $query
			->select('')
			->from('oc_product p')
			->join('LEFT JOIN','oc_product_description pd','pd.product_id = p.product_id')
			->orderBy('name')
			->offset($filter_data['pagination']->offset)
			->limit($filter_data['pagination']->limit)
			->all();

		// echo "<pre>";
		// var_dump($query);
		// echo "</pre>";

         return $results;
	}

	public static function getTotalProducts(){
		$total = self::find()->count();

		//  echo "<pre>";
		// var_dump($total);
		// echo "</pre>";

		return $total;
	}

	public static function getOne($id)
	{
	// start createCommand method
		// $query  = Yii::$app->db->createCommand('SELECT * FROM oc_product p LEFT JOIN oc_product_description pd ON p.product_id = pd.product_id  WHERE p.product_id = :id ')
		// ->bindValue(':id',$id)
		// ->queryOne();
	// end createCommand method

	// start builder method
		$query = new Query();
		$query = $query
			->select('')
			->from('oc_product p')
			->join('LEFT JOIN','oc_product_description pd','pd.product_id = p.product_id')
			->where(['p.product_id' => $id])
			->one();
	// end builder method

		$result = [
			'name' 			=> $query['name'],
			'model' 		=> $query['model'],
			'description' 	=> $query['description'],
			'qty' 			=> $query['quantity'],
			'price' 		=> $query['price'],
			'image' 		=> $query['image'],
		];

		
		// echo "<pre>";
		// var_dump($query);
		// echo "</pre>";
        return $result;
	}
}