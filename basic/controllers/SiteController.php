<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\helpers\html;
use yii\widgets\LinkPager;
use app\models\Mylist;
// use app\models\ContactForm;

class SiteController extends Controller
{
   

   

    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionHello()
    {
        $countresults = Mylist::getTotalProducts();
        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $countresults,
            ]);

        $filter_data = [
            'pagination' =>  $pagination,
        ];
        
        $results = Mylist::getAll($filter_data);

        foreach ($results as $result) {
            $data[] = array(
                'name' => $result['name'],
                'price' => $result['price'],
                'image' => $result['image'],
                'href' => 'two/'.$result['product_id'],
             );
        }

           //  echo "<pre>";
           // var_dump(Mylist::getTotalProducts());
           //  echo "</pre>";
       
        return $this->render('hello', [
            'results' => $data,
            'pagination' => LinkPager::widget(['pagination' => $pagination]),
            ]);
    }

    public function actionTwo($id)
    {

        $result = Mylist::getOne($id);

            $data = [
                'name' => $result['name'],
                'description' => html_entity_decode($result['description']),
                'model' => $result['model'],
                'qty' => $result['qty'],
                'price' => $result['price'],
                'image' => $result['image'],
            ];

        return $this->render('two', ['result' => $data]);
    }
}
