<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oc Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oc-product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Oc Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'product_id',
            'model',
            'sku',
            'upc',
            'ean',
            // 'jan',
            // 'isbn',
            // 'mpn',
            // 'location',
            // 'quantity',
            // 'stock_status_id',
            // 'image',
            // 'manufacturer_id',
            // 'shipping',
            // 'price',
            // 'points',
            // 'tax_class_id',
            // 'date_available',
            // 'weight',
            // 'weight_class_id',
            // 'length',
            // 'width',
            // 'height',
            // 'length_class_id',
            // 'subtract',
            // 'minimum',
            // 'sort_order',
            // 'status',
            // 'date_added',
            // 'date_modified',
            // 'viewed',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
