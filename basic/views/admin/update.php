<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OcProduct */

$this->title = 'Update Oc Product: ' . $model->product_id;
$this->params['breadcrumbs'][] = ['label' => 'Oc Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_id, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oc-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
