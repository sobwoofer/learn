<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OcProduct */

$this->title = $model->product_id;
$this->params['breadcrumbs'][] = ['label' => 'Oc Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oc-product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_id',
            'model',
            'sku',
            'upc',
            'ean',
            'jan',
            'isbn',
            'mpn',
            'location',
            'quantity',
            'stock_status_id',
            'image',
            'manufacturer_id',
            'shipping',
            'price',
            'points',
            'tax_class_id',
            'date_available',
            'weight',
            'weight_class_id',
            'length',
            'width',
            'height',
            'length_class_id',
            'subtract',
            'minimum',
            'sort_order',
            'status',
            'date_added',
            'date_modified',
            'viewed',
        ],
    ]) ?>

</div>
