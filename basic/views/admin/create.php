<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OcProduct */

$this->title = 'Create Oc Product';
$this->params['breadcrumbs'][] = ['label' => 'Oc Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oc-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
