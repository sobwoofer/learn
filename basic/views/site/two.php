<?php ?>
<h1><?= $result['name']; ?></h1>

<div class="thumbnail"><img src="<?= $result['image']; ?>"></div>
<div class="description">
	<table class="table table-responsive table-bordered table-striped">
		<thead>
			<tr>
				<td>Модель</td>
				<td>Цена</td>
				<td>Остаток</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?= $result['model']; ?></td>
				<td><?= $result['price']; ?></td>
				<td><?= $result['qty']; ?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr><td colspan="3">Описание</td></tr>
			<tr>
				<td colspan="3">
					<p><?= $result['description']; ?></p>
				</td>
			</tr>
		</tfoot>
	</table>
</div>