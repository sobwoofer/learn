<div class="content">
<?php
	// echo "<pre>";
 //    var_dump($results); 
	// echo "</pre>";
?>
	<?php foreach ($results as $result): ?>
		<div class="item col-sm-4">
			<div class="thumbnail"><img src="<?php echo $result['href']; ?>"></div>
			<div class="h2"><a href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></div>
			<div class="caption"><span>Цена:</span><span><?php echo $result['price']; ?></span></div>
			
		</div>

	<?php endforeach; ?>	

	<div class="col-sm-12 text-center">
		<?php echo $pagination; ?>
	</div>
	
</div>
